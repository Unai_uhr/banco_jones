--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cliente; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nombre text,
    nacimiento date NOT NULL,
    apellidos character varying(200),
    sexo character(1),
    email character varying(200) NOT NULL,
    telefono character varying(9) NOT NULL,
    dni character varying(9) NOT NULL,
    password character varying(200) NOT NULL,
    image bytea
);


ALTER TABLE public.cliente OWNER TO itb;

--
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO itb;

--
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;


--
-- Name: cuenta; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.cuenta (
    id integer NOT NULL,
    id_cliente integer NOT NULL,
    saldo numeric(10,2),
    num_cuenta text,
    creacion date
);


ALTER TABLE public.cuenta OWNER TO itb;

--
-- Name: cuenta_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.cuenta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_id_seq OWNER TO itb;

--
-- Name: cuenta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.cuenta_id_seq OWNED BY public.cuenta.id;


--
-- Name: movimientos; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.movimientos (
    id integer NOT NULL,
    fecha timestamp without time zone,
    cantidad numeric(100,2),
    id_origen integer,
    id_destino integer
);


ALTER TABLE public.movimientos OWNER TO itb;

--
-- Name: movimientos_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.movimientos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimientos_id_seq OWNER TO itb;

--
-- Name: movimientos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.movimientos_id_seq OWNED BY public.movimientos.id;


--
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.cliente_id_seq'::regclass);


--
-- Name: cuenta id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta ALTER COLUMN id SET DEFAULT nextval('public.cuenta_id_seq'::regclass);


--
-- Name: movimientos id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos ALTER COLUMN id SET DEFAULT nextval('public.movimientos_id_seq'::regclass);


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.cliente (id, nombre, nacimiento, apellidos, sexo, email, telefono, dni, password, image) FROM stdin;
8	Isaac	1986-11-16	Tort	h	unai.uhr@gmail.com	945172855	93691529V	$2y$10$37E4lVoB1i9bLS4PT6i9MeTyCXwBV3Z1NQACv5VUOqhmLbstFgFv6	\N
11	Unai	1986-03-14	Hernandez	h	unai.uhr@gmail.com	945172855	72747276R	$2y$10$nGXXnUJka4IP2Sdat6EN1ed/lDV5G/EAa/f7djSB8884QEhrtThZi	\N
\.


--
-- Data for Name: cuenta; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.cuenta (id, id_cliente, saldo, num_cuenta, creacion) FROM stdin;
7	11	0.00	00000000000000000000007	2021-02-08
8	11	0.00	00000000000000000000008	2021-02-08
9	11	0.00	00000000000000000000009	2021-02-08
10	11	265.00	00000000000000000000010	2021-02-24
5	11	300.00	00000000000000000000001	2021-02-03
6	11	400.00	00000000000000000000006	2021-02-03
\.


--
-- Data for Name: movimientos; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.movimientos (id, fecha, cantidad, id_origen, id_destino) FROM stdin;
6	2021-02-25 16:49:28.654962	5.00	5	10
7	2021-02-25 17:02:38.129746	10.00	5	10
8	2021-02-25 17:03:52.826011	10.00	5	10
9	2021-02-25 17:26:50.896355	55.00	5	10
10	2021-02-25 17:27:06.25117	100.00	5	10
11	2021-02-25 17:27:45.819463	100.00	5	10
12	2021-03-01 15:48:12.683281	100.00	5	6
13	2021-03-01 15:48:30.326848	100.00	5	6
14	2021-03-01 15:48:36.449988	100.00	5	6
15	2021-03-01 15:48:44.347012	100.00	5	6
\.


--
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.cliente_id_seq', 11, true);


--
-- Name: cuenta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.cuenta_id_seq', 10, true);


--
-- Name: movimientos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.movimientos_id_seq', 15, true);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (id);


--
-- Name: cuenta cuenta_unica; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_unica UNIQUE (num_cuenta);


--
-- Name: movimientos movimientos_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT movimientos_pkey PRIMARY KEY (id);


--
-- Name: cliente unico; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT unico UNIQUE (dni);


--
-- Name: cuenta id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- Name: movimientos id_origen; Type: FK CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT id_origen FOREIGN KEY (id_origen) REFERENCES public.cuenta(id);


--
-- PostgreSQL database dump complete
--

