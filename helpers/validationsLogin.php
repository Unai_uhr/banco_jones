<?php

function validateLogin(){

    $usuarioLogin = $_POST['dni'];
    $control_usuarioLogin = validarCampoNoVacio($usuarioLogin);
    $passwordLogin = $_POST['password'];
    $control_passwordLogin = validarCampoNoVacio($passwordLogin);
    if (!($control_usuarioLogin && $control_passwordLogin)) {
        $_POST['mensajeErrorLogin'] = "<p>ERROR: Campo de usuario o contraseña son erroneos.</p>";
        return false;
    }else{
        return true;
    }

}

function crearObjCliente($datos){
    return new Cliente($datos['name'] ,$datos['surname'] ,$datos['sexo'] ,$datos['fechaNacimiento'] ,$datos['dni'] ,$datos['tel'] ,$datos['mail'] ,$datos['password'], "") ;
}

?>
