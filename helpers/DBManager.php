<?php

require_once ('../config/config.php');

class DBManager extends PDO{

    public $server= SERVER;
    public $user= USER;
    public $pass= PASS;
    public $port= PORT;
    public $db=DB;

    private $conexion;
    private $manager;


    public function __construct()
    {
        $this->conectar();
    }

    private  final  function  conectar(){
        $conexio=null;
        try {
            if(is_array(PDO::getAvailableDrivers())){
                if(in_array('pgsql',PDO::getAvailableDrivers())){
                    $conexio = new PDO("pgsql:host=$this->server;port=$this->port;dbname=$this->db;user=$this->user;password=$this->pass");
                }else{
                    throw new PDOException('Error conexion');
                }
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
        $this->setConexion($conexio);
    }

    public final function getConexion(){
        return $this->conexion;
    }
    public final function setConexion($conexion){
        $this->conexion=$conexion;
    }

    public final  function  cerrarConexion(){
        $this->setConexion(null);
    }


}

?>
