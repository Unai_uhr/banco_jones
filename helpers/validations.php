<?php

function validateRegister(){

    $name=validarNombre($_POST['name']);
    if(!$name){
        $_POST['mensajeNombre']="<p>ERROR:  Campo nombre.</p>";
        $_POST['nombreUsuario']="";
    }else{
        $_POST['nombreUsuario']=$_POST['name'];
    }
    $surname=validarNombre($_POST['surname']);
    if(!$surname){
        $_POST['mensajeApellido']="<p>ERROR:  Campo apellido.</p>";
        $_POST['apellidoUsuario']="";
    }else{
        $_POST['apellidoUsuario']=$_POST['surname'];
    }
    $sexo=$_POST['sexo'];
    //echo $sexo;
    $_POST['sexoUsuario']="<option value='".$sexo."' selected='selected'>$sexo</option>";

    $nacimiento=validarNacimiento($_POST['fechaNacimiento']);
    if(!$nacimiento){
        $_POST['mensajeFecha']="<p>ERROR:  Campo fecha nacimiento.</p>";
        $_POST['nacimientoUsuario']="";
    }else{
        $_POST['nacimientoUsuario']=$_POST['fechaNacimiento'];
    }
    $dni=validarDni($_POST['dni']);
    if(!$dni){
        $_POST['mensajeDni']="<p>ERROR:  Formato de DNI incorreto.</p>";
        $_POST['dniUsuario']="";
    }else{
        $_POST['dniUsuario']=$_POST['dni'];
    }
    $telefono=validarTelefono($_POST['tel']);
    //echo $telefono;
    if(!$telefono){
        $_POST['mensajeTelf']="<p>ERROR:  Formato del telefono erroneo.</p>";
        $_POST['telUsuario']="";
    }else{
        $_POST['telUsuario']=$_POST['tel'];
    }
    $direccion=validarEmail($_POST['mail']);
    if(!$direccion){
        $_POST['mensajeMail']="<p>ERROR:  Formato del Email erroneo.</p>";
        $_POST['mailUsuario']="";
    }else{
        $_POST['mailUsuario']=$_POST['mail'];
    }
    $_POST['nacimientoUsuario']=$_POST['fechaNacimiento'];
    $pass=validarCampoNoVacio($_POST['password']);
    if(!$pass){
        $_POST['mensajePass']="<p>ERROR:  Password campo obligatorio.</p>";
        $_POST['passUsuario']="";
    }else{
        $_POST['passUsuario']=$_POST['password'];
    }
    //echo ("nombre: $name; apellidos: $surname; sexo: $sexo; Dni: $dni; Tel: $telefono; Mail: $email");
    if(validarNombre($_POST['name']) &&
        validarNombre($_POST['surname'])&&
        validarDni($_POST['dni']) &&
        validarTelefono($_POST['tel']) &&
        validarEmail($_POST['mail']) &&
        validarCampoNoVacio($_POST['password']) &&
        validarNacimiento($_POST['fechaNacimiento'])){


        $_POST['mensajeFinalOk']="<p>OK:  Todos los parámetros correctos.</p>";
        return true;
    }else return false;

}


function validarNombre($nombre){
    if( preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $nombre) ){
        return true;
    }else{
        return false;
    }
}

function validarNacimiento($fecha){
    //echo $fecha;
    if($fecha!=null){

        $cumpleanos = new DateTime($fecha);
        $hoy = new DateTime();
        $annos = $hoy->diff($cumpleanos);
        $years= $annos->y;
        if($years>=18){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function validarDni($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni, 0, -1);
    if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
        return true;
    }else{
        return false;
    }
}

function validarTelefono($telf){
    if(preg_match("/^([0-9])*$/", $telf)){
        return true;
    }else{
        return false;
    }
}

function validarEmail($mail)
{
    return (false !== strpos($mail, "@") && false !== strpos($mail, "."));
}

function validarCampoNoVacio($contenido) {
    if (strlen($contenido)!=0) {
        return true;
    }else{
        return false;
    }
}


?>
