<?php

require_once ('../helpers/DBManager.php');
require_once('../model/Cliente.php');
require_once ('../helpers/DBManager.php');
require_once('../model/Cuenta.php');
use DBManager;
use Cuenta, Cliente;

function getSaldo($cuenta){

    //error_log("CUENTAMODEL: getSaldo() ----->".$cuenta);
    $manager = new DBManager();
    try {
        $sql = "SELECT saldo FROM cuenta WHERE num_cuenta=:num_cuenta";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':num_cuenta',$cuenta);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();

        return $rt[0]['saldo'];



    }catch(PDOException $e){
        echo $e->getMessage();
    }

}
function getLastId(){
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM cuenta ORDER BY id DESC limit 1";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (sizeof($rt)>0){
            return $rt[0]['id'];
        }else{
            return 0;
        }
        $manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function getUserId($dni)
{
    $manager = new DBManager();
    //error_log("GETUSER ID DNI --------------->".$dni);
    try {
        $sql = "SELECT id FROM cliente WHERE dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //error_log("GETUSER ID --------------->".$rt[0]['id']);
        if (sizeof($rt) > 0) {
            return $rt[0]['id'];
        } else {
            return 0;
        }

        $manager->cerrarConexion();
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}
function createAccount($user){
    //error_log("USER ID --------------->");
    $manager = new DBManager();
    $rt = null;
    try {
        //recuperar el último id de cuenta y con el id generamos el iban que tiene 24 dígitos
        $lastId=getLastId()+1;
        //error_log("LAST ID --------------->".$lastId);
        $len=strlen($lastId);
        $iban='';
        for ($i=1;$i<24-$len;$i++){
            $iban.='0';
        }
        $iban.=$lastId;
        //error_log("CREATEACOUNT USER DNI --------------->".unserialize($user)->getDni());

        $id=getUserId(unserialize($user)->getDni());
        error_log("USER ID --------------->".$id);
        //Insertamos en base de datos
        $sql = "INSERT INTO cuenta (num_cuenta,id_cliente,saldo,creacion) VALUES (:num_cuenta,:id_cliente,0,now())";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':num_cuenta',$iban);
        $stmt->bindParam(':id_cliente',$id);
        $stmt->execute();
        $manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }


}
function getAccounts($dni){
    $manager = new DBManager();

    //error_log("GET-ACOUNTS--------->".$dni);

    try {
        $sql = "SELECT * FROM cuenta WHERE id_cliente=:id_cliente";
        $stmt = $manager->getConexion()->prepare($sql);
        $id_cliente=getUserId($dni);
        //error_log("GET-ACOUNTS ID CLIENTE--------->".$id_cliente);
        $stmt->bindParam(':id_cliente',$id_cliente);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rt;

        $manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function existeCuenta($cuenta){
    $num=explode("/",$cuenta);
    //var_dump($num[0]);
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cuenta WHERE num_cuenta=:num_cuenta";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':num_cuenta',$num[0]);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();

        if (count($rt)>0){
            return true;
        }else{
            return false;
        }

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}


function transfer($origen, $destino, $cantidad)
{
    error_log("ESTOY EN TRANSFER  origen ".$origen." -----destino ".$destino." -----cantidad ".$cantidad);
    $manager = new DBManager();
    $num=explode("/",$origen);
    $origen=$num[0];

   try {

        //Obtener id's de cuentas-->
        $sqlOrigen=" SELECT id FROM cuenta WHERE num_cuenta=:origen";
        $stmt = $manager->getConexion()->prepare($sqlOrigen);
        $stmt->bindParam(':origen', $origen);
        $stmt->execute();
        $idOrigen = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $sqlDestino=" SELECT id FROM cuenta WHERE num_cuenta=:destino";
        $stmt = $manager->getConexion()->prepare($sqlDestino);
        $stmt->bindParam(':destino', $destino);
        $stmt->execute();
        $idDestino = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Controlar si hay saldo suficiente en la cuenta origen
        $sqlComprobarSiHaySaldo=" SELECT saldo FROM cuenta WHERE id=:id";
        $stmt = $manager->getConexion()->prepare($sqlComprobarSiHaySaldo);
        $stmt->bindParam(':id', $idOrigen[0]['id']);
        $stmt->execute();
        $saldo = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($saldo[0]['saldo']>=$cantidad){
            $sql = "INSERT INTO movimientos (fecha,cantidad,id_origen,id_destino) VALUES (now(),:cantidad, :origen,:destino)";
            $stmt = $manager->getConexion()->prepare($sql);
            $stmt->bindParam(':origen', $idOrigen[0]['id']);
            $stmt->bindParam(':destino', $idDestino[0]['id']);
            $stmt->bindParam(':cantidad', $cantidad);
            $stmt->execute();

            $sql = "UPDATE cuenta SET saldo = saldo - $cantidad WHERE num_cuenta=:origen";
            $stmt = $manager->getConexion()->prepare($sql);
            $stmt->bindParam(':origen', $origen);
            $stmt->execute();

            $sql = "UPDATE cuenta SET saldo = saldo + $cantidad WHERE num_cuenta=:destino";
            $stmt = $manager->getConexion()->prepare($sql);
            $stmt->bindParam(':destino', $destino);
            $stmt->execute();
        }else{
            echo("SALDO INSUFICIENTE");
            error_log("SALDO INSUFICIENTE");
        }


        $manager->cerrarConexion();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getMovimientos($cuenta)
{
    error_log("GETMOVIMIENTOS ----".$cuenta);
    $manager = new DBManager();
    $sqlOrigen=" SELECT id FROM cuenta WHERE num_cuenta=:origen";
    $stmt = $manager->getConexion()->prepare($sqlOrigen);
    $stmt->bindParam(':origen', $cuenta);
    $stmt->execute();
    $idOrigen = $stmt->fetchAll(PDO::FETCH_ASSOC);
    error_log("GETMOVIMIENTOS ID ----".$idOrigen[0]['id']);

    try {
        $sql = "SELECT * FROM movimientos WHERE id_origen=:id_origen";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':id_origen', $idOrigen[0]['id']);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();
        error_log("GETMOVIMIENTOS RESULTADO----".$rt[0]['fecha']);
        return $rt;

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}




?>