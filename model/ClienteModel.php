<?php

require_once ('../helpers/DBManager.php');
require_once('../model/Cliente.php');
use DBManager;
use Cliente;

function insertCliente($cliente){

    $dbManager=new DBManager();

    try {
        $sql ="INSERT INTO cliente(nombre,nacimiento,apellidos,sexo, email,telefono, dni, password)VALUES (:nombre,:nacimiento,:apellidos,:sexo, :email,:telefono, :dni, :password)";

        $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=>10]);

        $stmt=$dbManager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$cliente->getNombre());
        $stmt->bindParam(':nacimiento',$cliente->getFechaNacimiento());
        $stmt->bindParam(':apellidos',$cliente->getApellido());
        $stmt->bindParam(':sexo',$cliente->getSexo());
        $stmt->bindParam(':email',$cliente->getEmail());
        $stmt->bindParam(':telefono',$cliente->getTelefono());
        $stmt->bindParam(':dni',$cliente->getDni());
        $stmt->bindParam(':password',$password);

        echo ("SQL:____"+$sql);

        if($stmt->execute()){
            echo "Usuario insertado";
        }else{
            echo "Usuario Mal insertado";
        }


    }catch (PDOException $e ){
        echo $e->getMessage();
    }


}



function getUserHash($dni){

    $conexion=new DBManager();
    try {
        error_log('LOG: ClienteModel - getUserHash - Init query ');
        $sql= "SELECT * FROM cliente WHERE dni=:dni";

        //echo $dni;
        $stmt=$conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        //echo $sql;
        $stmt->execute();
        $result= $stmt->fetchAll(PDO::FETCH_ASSOC);
        error_log('LOG: ClienteModel - getUserHash - Finish Query');
        return $result[0];


    }catch (PDOException $e){
        echo $e->getMessage();
    }
}


function updateCliente($image, $clienteDNI){
    error_log("MI UPDATE-> updateCliente() ".$clienteDNI);
    $conexion=new DBManager();
    try {
        $sql="UPDATE cliente SET image=:img WHERE dni=:dni";
        $stmt=$conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt->bindParam(':dni',$clienteDNI);
        error_log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx: ".$sql);
        $stmt->execute();

    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function selectCliente($clienteDNI){
    $conexion=new DBManager();
    try {
        $sql="SELECT * FROM cliente WHERE dni=:dni";

        $stmt=$conexion->getConexion()->prepare($sql);
        error_log( "CLIENTE DNI---->".$clienteDNI);
        $stmt->bindParam(':dni',$clienteDNI);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $obj= new Cliente($result[0]['nombre'],$result[0]['nacimiento'],$result[0]['apellidos'],$result[0]['sexo'],$result[0]['email'],$result[0]['telefono'],$result[0]['dni'],$result[0]['password'],$result[0]['image']);
        return $obj;
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

?>
