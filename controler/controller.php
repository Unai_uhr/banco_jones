<?php

require_once ('../helpers/validations.php');
require_once ('../helpers/validationsLogin.php');
require_once ('../model/Cliente.php');
require_once ('../model/ClienteModel.php');
require_once ('../model/Cuenta.php');
require_once ('../model/CuentaModel.php');
require_once ('../helpers/rellenarCampos.php');
use Cliente, Cuenta;

$clienteDNI ="";


if(isset($_POST['submit'])){
    /*if ( $_POST['switch_lang'] == 'switch_lang' ){
        $lang= $_POST['lang'];
        setcookie('lang', $lang, time()+60*60*24*30, '/', 'localhost');
        header('Location: '. $_SERVER['HTTP_REFERER']);
    }*/

    if ( $_POST['control'] == 'register' ){

        if (validateRegister()){

            $cliente = new Cliente($_POST['name'] ,$_POST['surname'] ,$_POST['sexo'] ,$_POST['fechaNacimiento'] ,$_POST['mail'] ,$_POST['tel'],$_POST['dni'],$_POST['password'], "") ;
            error_log("CLIENTE:---->".$cliente->getDni());
            insertCliente($cliente);
            header('Location: ../views/login.php');
        }else
            require_once ('../views/register.php');

    }

    /*---------------------------LOGIN.HTML-------------------------------------------*/
    if ( $_POST['control'] == 'login' ) {

        if (validateLogin()) {//login correcto
            $result=getUserHash($_POST['dni']);
            error_log("RESULTADO SELECT LOGIN: ".$result['nombre']);
            $cliente=new Cliente($result['nombre'],$result['apellidos'],$result['sexo'],$result['nacimiento'],$result['email'],$result['telefono'],$result['dni'],$result['password'],$result['image']);
            $hash=$cliente->getPassword();

            if (password_verify($_POST['password'], $hash)) {
                error_log("PASS VERIFICADO: ");
                session_start();
                $_SESSION['user'] = serialize($cliente);
                //error_log("LOGIN>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<".$_SESSION['user']);
                //error_log(unserialize($_SESSION['user'])->getNombre() . "<<<<<<<<<<<<<<<<<");
                header('Location: ../views/init.php');
            }




        } else {
            echo "<script>alert('PASS incorrecto:' );</script>";
            //login incorrecto
            require_once('../views/login.php');
            header('Location: ../views/login.php');

        }
    }
        /*---------------------------PROFILE.HTML-------------------------------------------*/

    if ( $_POST['control'] == 'profile' ){
        session_start();
        //esto tendria que ser cuando muestra el profile
        $check= getimagesize($_FILES['foto']['tmp_name']);
        $fileName= $_FILES['foto']['name'];
        $fileName= $_FILES['foto']['size'];
        $fileName= $_FILES['foto']['type'];

        //error_log("PROFILE : ".$_SESSION['user']->getDni());

        if($check != false){
            error_log("PROFILE update: ".$_SESSION['user']->getDni());
            $image = file_get_contents($_FILES['foto']['tmp_name']);
            updateCliente($image,$_SESSION['user']->getDni());//UPDATE CLIENTE
        }
        //mostramos la imagen
        $objCliente=selectCliente($_SESSION['user']->getDni());
        ob_start();
        fpassthru($objCliente->getImage());
        $data = ob_get_contents();
        ob_end_clean();

        $img= "data:image/*;base64,".base64_encode($data);
        echo "<img src='".$img."'/>";

    }
    /*---------------------------CREARCUENTA.HTML-------------------------------------------*/
    if($_POST['control']=='create'){
        session_start();
        createAccount($_SESSION['user']);
        //error_log("CREATE CUENTA>>>>>>>>>>>>>>>>>".$_SESSION['user']);
        header('Location: ../views/init.php');

    }
    /*---------------------------SELECT ACOUNT-------------------------------------------*/
    if($_POST['control']=='select_account'){
        error_log("ESTOY SELECT ACOUNT--------->");
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo']=$saldo;
        error_log("ESTOY SELECT ACOUNT--------->".$_SESSION['saldo']);
        header("Location: ../views/query.php");

    }
    /*------------------------------TRANSFER ____________________________-*/

    if($_POST['control']=='transfer') {
        if (existeCuenta($_POST['num_cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
            transfer($_POST['num_cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
        }
        require_once('../views/init.php');
    }

    /*------------------------------Query ____________________________-*/
    if($_POST['control']=='query') {

        session_start();
        $_SESSION['saldo']=getSaldo($_POST['cuentas']);
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);


        header("Location: ../views/query.php");
    }






}


?>
