<html>
<head>
    <title>Banco Jones_LOGIN</title>
    <link  rel="stylesheet" href="../assets/css/style.css">
    <script src="../assets/js/validacioneRegister.js"></script>

</head>

<body>

<main>
    <h1>Banco Jones</h1>
    <form action="./../controler/controller.php" method="POST" onsubmit="">
        <input name="control" value="login" type="hidden"/>
        <input name="dni" type="text" placeholder="<?php echo _("Usuario --> DNI")?>"/>
        <input name="password" type="password" placeholder="<?php echo _("Password")?>"/>
        <input name="submit" type="submit" value="submit"/>
        <a href="register.php"> <?php echo _("No tiene cuenta. REGISTRESE!")?></a>
    </form>

    <div class="mensajesError">
        <?php
            if(isset($_POST['mensajeErrorLogin'])){
                echo $_POST['mensajeErrorLogin'];
            }
        ?>
    </div>
</main>

</body>
</html>
