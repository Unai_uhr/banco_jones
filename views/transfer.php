<html>
<head>
    <title>Banco Jones_WELCOME</title>
    <link  rel="stylesheet" href="../assets/css/style.css">
    <script src="../assets/js/validacioneRegister.js"></script>

</head>

<body>

    <main>
        <header>
            <h1>BANCO JONES </h1>
            <nav>
                <a href="init.php">HOME</a>
                <a href="profile.php">PERFIL</a>
                <a href="query.php">MOVIMIENTOS</a>
                <span><img src="../assets/img/search.ico" alt=""></span>
                <span><img src="../assets/img/castellano.ico" alt=""><img src="../assets/img/catalan.ico" alt=""><img src="../assets/img/ingles.ico" alt=""></span>
                <a href="login.php"><img src="../assets/img/logout.ico" alt=""></a>

            </nav>

        </header>

        <h2>
        TRANSFERENCIA
        </h2>
        <table>
            <tr>
                <th>Nº de cuenta ORIGEN</th>
                <th>Saldo</th>
                <th>Nº de cuenta DESTINO</th>
                <th>Saldo</th>
            </tr>
                <tr>
                    
                <th style="width:40%">
                    <select  name="cuentaOrigen" id="cuentaOrigen">
                        <option value="seleccioneCuenta" selected>Seleccione Nº de cuenta</option>
                        <option value="numCuenta" >Nº de cuenta</option>
                        <option value="numCuenta" >Nº de cuenta</option>
                    </select>
                </th>
                <th style="width:10%">
                    <input name="saldoOrigen" type="text" readonly placeholder="Saldo">
                </th>
                <th style="width:40%">
                    <select name="cuentaDestino" id="cuentaDestino">
                        <option value="seleccioneCuenta" selected>Seleccione Nº de cuenta</option>
                        <option value="numCuenta" >Nº de cuenta</option>
                        <option value="numCuenta" >Nº de cuenta</option>
                    </select>
                </th>
                <th style="width:10%">
                     <input  name="saldoDestino" type="text" readonly placeholder="Saldo">
                </th>
            </tr> 
            <tr>
                <th colspan="4"><a href="transfer.php"><img src="../assets/img/ok.ico" alt="trans"></a><a href="transfer.php"><img src="../assets/img/cancel.ico" alt="trans"></a></th>

            </tr>        
        </table>



    </main>

</body>
</html>
