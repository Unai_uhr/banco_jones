<?php require_once ('../helpers/i18n.php');?>
<html>
    <head>
        <title><?php echo _("Banco Jones_REGISTER")?></title>
        <link  rel="stylesheet" href="../assets/css/style.css"/>
        <script src="../assets/js/validacioneRegister.js"></script>

    </head>

    <body>
    <?php echo _("hola")?>
    <?php require_once ('header.php');?>
    <main>
        <h1>Banco Jones</h1>
        <form name="registerForm" action="./../controler/controller.php" method="POST">
            <input name="control" value="register" type="hidden"/>
            <input name="name" type="text" placeholder="<?php echo _("Nombre")?>" onfocusout="validarNombre('name')"  value='<?php if(isset($_POST['nombreUsuario'])){ echo $_POST['nombreUsuario'];}?>'/>
            <input name="surname" type="text" placeholder="<?php echo _("Apellido")?>" onfocusout="validarNombre('surname')" value='<?php if(isset($_POST['apellidoUsuario'])){ echo $_POST['apellidoUsuario'];}?>'/>
            <select name="sexo">
                <?php
                if(isset($_POST['sexoUsuario'])){
                echo $_POST['sexoUsuario'];
                }
                ?>
                <option value="h">Hombre</option>
                <option value="m">Mujer</option>
            </select>
            <input name="fechaNacimiento" type="date" value='<?php if(isset($_POST['nacimientoUsuario'])){ echo $_POST['nacimientoUsuario'];}?>'/>
            <input name="dni" type="text" placeholder="<?php echo _("DNI")?>" title="Ejemplo: 12345678A" value='<?php if(isset($_POST['dniUsuario'])){ echo $_POST['dniUsuario'];}?>'/>
            <input name="tel" type="tel" placeholder="<?php echo _("Telefono")?>" maxlength="9" value='<?php if(isset($_POST['telUsuario'])){ echo $_POST['telUsuario'];}?>'/>
            <input name="mail" type="email" placeholder="<?php echo _("Email")?>" value='<?php if(isset($_POST['mailUsuario'])){ echo $_POST['mailUsuario'];}?>'/>
            <input name="password" type="password" placeholder="<?php echo _("Password")?>"  value='<?php if(isset($_POST['passUsuario'])){ echo $_POST['passUsuario'];}?>'/>
            <input name="submit" type="submit" value="submit"/>
        </form>

        <div class="mensajesError">
            <?php
            if(isset($_POST['mensajeNombre'])){
                echo $_POST['mensajeNombre'];
            }
            if(isset($_POST['mensajeApellido'])){
                echo $_POST['mensajeApellido'];
            }
            if(isset($_POST['mensajeSexo'])){
                echo $_POST['mensajeSexo'];
            }
            if(isset($_POST['mensajeFecha'])){
                echo $_POST['mensajeFecha'];
            }
            if(isset($_POST['mensajeDni'])){
                echo $_POST['mensajeDni'];
            }
            if(isset($_POST['mensajeTelf'])){
                echo $_POST['mensajeTelf'];
            }
            if(isset($_POST['mensajeMail'])){
                echo $_POST['mensajeMail'];
            }
            if(isset($_POST['mensajePass'])){
                echo $_POST['mensajePass'];
            }

            ?>
        </div>
        <div class="mensajesOk">
            <?php
            if(isset($_POST['mensajeFinalOk'])){
            echo $_POST['mensajeFinalOk'];
            }
            ?>
        </div>


    </main>

    </body>
</html>


