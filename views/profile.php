<?php require_once ('../helpers/rellenarCampos.php');?>
<?php require_once ('../model/Cliente.php');?>
<html>
<head>
    <title>Banco Jones_PROFILE</title>
    <link  rel="stylesheet" href="../assets/css/style.css">
    <script src="../assets/js/validacioneRegister.js"></script>

</head>

<body>
<?php session_start(); if(isset( $_SESSION['user'] )){ ?>
<main>
    <h1>Banco Jones</h1>
    <nav>
        <a href="init.php">HOME</a>
        <span><img src="../assets/img/search.ico" alt=""></span>
        <span><img src="../assets/img/castellano.ico" alt=""><img src="../assets/img/catalan.ico" alt=""><img src="../assets/img/ingles.ico" alt=""></span>
        <a href="login.php"><img src="../assets/img/logout.ico" alt=""></a>

    </nav>
    <form class="formProfile" action="./../controler/controller.php" method="post" enctype="multipart/form-data">
        <input name="control" type="text" value="profile"  hidden/>
        <div>
            <input class="fotoUsuario" name="photo" type="image" src="../assets/img/fotoPorDefecto.png" placeholder="Su foto aquí"/>
            <div class="conjuntoInputBoton">
                <input name="foto" type="file" placeholder="Nombre de su foto"/>
            </div>
        </div>
        
        <input name="name" type="text" placeholder="Nombre"  readonly value="<?php  echo unserialize($_SESSION['user'])->getNombre();?>"/>
        <input name="surname" type="text" placeholder="Apellidos" readonly value="<?php  echo unserialize($_SESSION['user'])->getApellido();?>"/>
        <input name="sexo" type="text" placeholder="sexo" readonly readonly value="<?php  echo unserialize($_SESSION['user'])->getSexo();?>"/>
        <input name="fechaNacimiento" type="date" readonly readonly value="<?php  echo unserialize($_SESSION['user'])->getFechaNacimiento();?>"/>
        <input name="dni" type="text" placeholder="DNI" readonly readonly value="<?php  echo unserialize($_SESSION['user'])->getDni();?>" />
        <input name="tel" type="tel" placeholder="Teléfono" readonly value="<?php  echo unserialize($_SESSION['user'])->getTelefono();?>"/>
        <input name="mail" type="email" placeholder="Email" readonly value="<?php  echo unserialize($_SESSION['user'])->getEmail();?>"/>
        <input name="password" type="password" placeholder="Password"/>


        <input name="submit" type="submit" value="ACTUALIZAR DATOS"/>
    </form>
</main>
<?php }else{
   //reenvio a login
}?>
</body>
</html>
