<html>
<head>
    <title>Banco Jones_WELCOME</title>
    <link  rel="stylesheet" href="../assets/css/style.css">
    <script src="../assets/js/validacioneRegister.js"></script>

</head>

<body>
<h2>Seleccione la cuenta de la que quiere ver los movimientos</h2>
<form action="./../controler/controller.php" method="post">
    <label>Número de cuenta:</label>
    <select name="cuentas">

        <?php
        require_once('../model/CuentaModel.php');
        session_start();
        $dni=unserialize($_SESSION['user'])->getDni();
        $accounts=getAccounts($dni);
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["num_cuenta"] ?></option>
        <?php }?>
    </select>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="query"/>
</form>


<?php
session_start();
if (isset($_SESSION['saldo'])) {
    echo '<table>';
    echo "<tr><th>Saldo " . $_SESSION['saldo'] . '</th></tr>';
    echo '</table>';
}

if (isset($_SESSION['lista'])) {
    $movimientos=$_SESSION['lista'];
    echo '<table class="default" rules="all" frame="border">';
    echo '<tr>';
    echo '<th>origen</th>';
    echo '<th>destino</th>';
    echo '<th>hora</th>';
    echo '<th>cantidad</th>';
    echo '</tr>';
    for ($i=0;$i<count($movimientos);$i++){
        echo '<tr>';
        echo '<td>'.$movimientos[$i]['id_origen'].'</td>';
        echo '<td>'.$movimientos[$i]['id_destino'].'</td>';
        echo '<td>'.$movimientos[$i]['fecha'].'</td>';
        echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
        echo '</tr>';
    }
    echo '</table>';

}

?>



</body>
</html>